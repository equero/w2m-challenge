# Nombre del Proyecto

Breve descripción del proyecto...

## Instalar y probar

Para instalar las dependencias y arrancar el proyecto, sigue los siguientes pasos:

```
git clone https://url-del-repositorio.git
cd nombre-del-proyecto
npm install
ng serve
```


Esto iniciará el servidor de desarrollo y podrás acceder a la aplicación en `http://localhost:4200/`.

## Consideraciones iniciales
El backend que usado los dos primeros dias ha funcionado correctamente pero en las ultimas pruebas ,las peticiones de delete y put, estan un poco bugueadas, pero el codigo si que esta para que podais comprobar que logicamente si esta bien estructurado

## Requisitos

Aquí puedes listar los requisitos del proyecto. Por ejemplo:

- [x] CRUD
- [ ] Test Cases ( No puedo ejectuar test en el ordenador donde he desarollado esta prueba. Hare un update a lo largo de la tarde con los test pero sin poder probarlos)
- [x] Loader global para las peticiones
- [x] SPA
- [x] FIltrado de datos con debouncing
- [x] Notificaciones al usuario de las peticiones
- [x] Reactive development
- [x] Angular Material
- [x] Tipado de codigo
- [x] Estructura modular

## Preguntas frecuentes

### ¿Cuanto tiempo has dedicado a la prueba?

Para hacer la mayoria de la prueba apenas han sido unas 8 horas pero he tenido muchisimos problemas por el camino. DOnde he desarollado la prueba, en una steamdeck, tiene un sistema operativo solo de lectura (arch linux distro) y me ha costado mucho configurar un entorno de desarollo para termianr todo lo posible de la prueba.

### ¿Porque no has hecho los test?

Como ya he comentado no he podido ejecutar los test en la maquina donde he desarollado

### ¿El home esta vacio?

No tenia ninguna intencion de usar el Home, era solo para mostrar fisicamente que la aplicacion no se recarga y esta usando las estrategias de SPA

### ¿Que backend usas?

Uso una app online gratuita llamada mockapi para poder mostrar datos.

## Changelog

869d1f9 - feat: Create all views in their most basic version (Enrique Quero, 4 days ago)
8c96934 - feat: Begin project by creating essential app files (Enrique Quero, 4 days ago)
41ab5c1 - initial commit (Enrique Quero, 4 days ago)
