export const environment = {
  production: true,
  useFakeAPI: false,
  apiUrl: 'https://65fa1af53909a9a65b19df07.mockapi.io/api'
};
