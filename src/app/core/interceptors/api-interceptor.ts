import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpClient
} from '@angular/common/http';
import { Observable, map } from 'rxjs';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  constructor(private httpClient: HttpClient) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (req.method === 'GET' && req.url.includes('/heroes/list')) {
      if (req.params.has('name')) {
       
      }

      req = req.clone({
        url: 'assets/data/heroes-list.json'
      });
    }

    return next.handle(req);
  }
}