import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { BehaviorSubject } from 'rxjs';
import { Superheroe } from '../models/heroes';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class DataStoreService {
  private _superHeroesSource = new BehaviorSubject<Superheroe[]>([]);
  public superHeroes$ = this._superHeroesSource.asObservable();

  constructor(private dataService: DataService, private _snackBar: MatSnackBar) { }

  private handleSubscriptionSuccess(message: string, loadSuperHeroes: boolean = true): void {
    this._snackBar.open(message, '', { duration: 3000 });
    if (loadSuperHeroes) {
      this.loadSuperHeroes();
    }
  }

  private handleSubscriptionError(error: string): void {
    this._snackBar.open(error, '', { duration: 3000 });
  }

  loadSuperHeroes(): void {
    this.dataService.getAllSuperHeroes().subscribe({
      next: (superheroes) => this._superHeroesSource.next(superheroes),
      error: () => this.handleSubscriptionError('Error al cargar superhéroes'),
    });
  }

  loadSuperHeroesByPage(page: number, pageSize: number): void {
    this.dataService.getAllSuperHeroesByPage(page, pageSize).subscribe({
      next: (superheroes) => this._superHeroesSource.next(superheroes),
      error: () => this.handleSubscriptionError('Error al cargar superhéroes por página'),
    });
  }

  loadSuperHeroesByName(name: string): void {
    this.dataService.getSuperHeroByName(name).subscribe({
      next: (superheroes) => this._superHeroesSource.next(superheroes),
      error: () => this.handleSubscriptionError('Error al buscar superhéroe por nombre'),
    });
  }

  deleteSuperHeroesById(id: number, name: string): void {
    this.dataService.deleteSuperHeroeById(id, name).subscribe({
      next: () => this.handleSubscriptionSuccess('Superhéroe borrado correctamente!'),
      error: () => this.handleSubscriptionError('Error al borrar superhéroe'),
    });
  }

  createOrUpdateSuperHeroe(superheroe: Superheroe): void {
    const operation = superheroe.id ? this.dataService.editSuperHeroe(superheroe) : this.dataService.createSuperHeroe(superheroe);

    operation.subscribe({
      next: () => this.handleSubscriptionSuccess('Superhéroe guardado con éxito!', false),
      error: () => this.handleSubscriptionError('Error al guardar el superhéroe'),
    });
  }
}
