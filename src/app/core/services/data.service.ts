import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, finalize, map, of, throwError } from 'rxjs';
import { environment } from '../../../environments/environment.development';
import { Personaje } from '../models/heroes';
import { LoaderService } from './loader.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private apiUrl = environment.apiUrl;
  private heroesUrl = '/heroes';
  
  constructor(private http: HttpClient, private loaderService: LoaderService) {}
  
  private getRequest<T>(path: string, params?: HttpParams): Observable<T> {
    this.loaderService.show();
    return this.http.get<T>(`${this.apiUrl}${path}`, { params }).pipe(
      catchError(() => of([] as unknown as T)),
      finalize(() => this.loaderService.hide())
    );
  }

  private executeDelete<T>(path: string, params?: HttpParams): Observable<T> {
    this.loaderService.show();
    return this.http.delete<T>(`${this.apiUrl}${path}`, { params }).pipe(
      catchError(() => of([] as unknown as T)),
      finalize(() => this.loaderService.hide())
    );
  }

  private executePut<T>(path: string, body?: any): Observable<T> {
    this.loaderService.show();
    return this.http.put<T>(`${this.apiUrl}${path}`, body).pipe(
      catchError(error => throwError(() => new Error('Error al crear o actualizar el superhéroe'))),
      finalize(() => this.loaderService.hide())
    );
  }

  getAllSuperHeroes(): Observable<Personaje[]> {
    return this.getRequest<Personaje[]>(this.heroesUrl);
  }

  getAllSuperHeroesByPage(page: number, pageSize: number): Observable<Personaje[]> {
    let params = new HttpParams().set('page', page.toString()).set('limit', pageSize.toString());
    return this.getRequest<Personaje[]>(this.heroesUrl, params);
  }

  getSuperHeroByName(name: string): Observable<Personaje[]> {
    let params = new HttpParams().set('nombre', name);
    return this.getRequest<Personaje[]>(this.heroesUrl, params);
  }

  deleteSuperHeroeById(id: number, name: string): Observable<{}> {
    let params = new HttpParams().set('id', id.toString()).set('nombre', name);
    return this.executeDelete<{}>(`${this.heroesUrl}/${id}`, params);
  }

  createSuperHeroe(superheroe: Personaje): Observable<Personaje> {
    return this.executePut<Personaje>(this.heroesUrl, superheroe);
  }

  editSuperHeroe(superheroe: Personaje): Observable<Personaje> {
    return this.executePut<Personaje>(`${this.heroesUrl}/${superheroe.id}`, superheroe);
  }
}
