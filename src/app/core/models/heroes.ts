export interface Personaje {
  id: number;
  nombre: string;
  nombre_real: string;
  genero: string;
  poderes: string[];
  alias: string[];
  equipo: string;
  ciudad: string;
  nacionalidad: string;
  altura: string;
  peso: string;
  color_ojos: string;
  color_cabello: string;
  primera_aparicion: string;
}

export interface Superheroe extends Personaje {}

export interface Supervillano extends Personaje {}