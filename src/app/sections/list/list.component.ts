import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, Subscription, debounceTime, distinctUntilChanged, fromEvent, map } from 'rxjs';
import { Superheroe } from '../../core/models/heroes';
import { DataStoreService } from '../../core/services/data-store.service';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { SuperHeroeFormComponent } from '../super-heroe-form/super-heroe-form.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrl: './list.component.scss'
})

export class ListComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('searchInput') searchInput!: ElementRef;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  displayedColumns: string[] = ['id', 'nombre', 'nombre_real', 'genero', 'alias', 'equipo', 'ciudad', 'nacionalidad', 'altura', 'peso', 'primera_aparicion', 'actions'];
  tableData = new MatTableDataSource<Superheroe>([]);
  searchTerm: string = '';
  currentPage: number = 1;
  private subscription: Subscription = new Subscription();
  
  constructor(public dataStoreService: DataStoreService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.dataStoreService.superHeroes$.subscribe(data => {
      this.tableData.data = data;
    });
    this.dataStoreService.loadSuperHeroes();
  }

  ngAfterViewInit(): void {
    this.tableData.paginator = this.paginator;
    
    fromEvent(this.searchInput?.nativeElement, 'input')
      .pipe(
        map((event: any) => event.target.value),
        debounceTime(300),
        distinctUntilChanged()
      )
      .subscribe((text: string) => {
        this.dataStoreService.loadSuperHeroesByName(this.searchTerm);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  edit(element: Superheroe) {
    this.openEditSuperheroDialog(element);
  }
  
  delete(element: Superheroe) {
    this.openDeleteDialog(element.id, element.nombre);
  }

  openDeleteDialog(id: number, name: string): void {
    let dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.dataStoreService.deleteSuperHeroesById(id, name);
      }
    });
  }

  openNewSuperheroDialog() {
    const dialogRef = this.dialog.open(SuperHeroeFormComponent, {
      width: '600px',
    });
  
    dialogRef.afterClosed().subscribe(result => {
      console.log('El diálog fue cerrado');
    });
  }

  openEditSuperheroDialog(element: Superheroe) {
    const dialogRef = this.dialog.open(SuperHeroeFormComponent, {
      width: '600px',
      data: element,
    });
  
    dialogRef.afterClosed().subscribe(result => {
      console.log('El diálog fue cerrado');
    });
  }
}
