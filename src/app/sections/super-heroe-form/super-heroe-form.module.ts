import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuperHeroeFormComponent } from './super-heroe-form.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    SuperHeroeFormComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class SuperHeroeFormModule { }
