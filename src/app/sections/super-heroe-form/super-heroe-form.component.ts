import { AfterViewInit, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataStoreService } from '../../core/services/data-store.service';
import { Superheroe } from '../../core/models/heroes';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-super-heroe-form',
  templateUrl: './super-heroe-form.component.html',
  styleUrls: ['./super-heroe-form.component.scss']
})
export class SuperHeroeFormComponent implements OnInit, AfterViewInit {
  superheroForm: FormGroup;
  
  constructor(
    private fb: FormBuilder, 
    private dataStoreService: DataStoreService, 
    @Inject(MAT_DIALOG_DATA) public data: Superheroe | null,
    private cdr: ChangeDetectorRef
  ) { 
    this.superheroForm = this.createFormGroup();
  }

  ngOnInit(): void {
    if (this.data) {
      this.superheroForm.patchValue(this.data);
    }
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
  }

  createFormGroup(): FormGroup {
    return this.fb.group({
      id: ['', Validators.required],
      nombre: ['', Validators.required],
      nombre_real: ['', Validators.required],
      genero: ['', Validators.required],
      equipo: ['', Validators.required],
      ciudad: ['', Validators.required],
      nacionalidad: ['', Validators.required],
      altura: ['', Validators.required],
      peso: ['', Validators.required],
      color_ojos: ['', Validators.required],
      color_cabello: ['', Validators.required],
      primera_aparicion: ['', Validators.required],
      alias: ['', Validators.required],
    });
  }

  createNewSuperHeroe(): void {
    if (this.superheroForm.valid) {
      this.dataStoreService.createOrUpdateSuperHeroe(this.superheroForm.value);
    }
  }
}
