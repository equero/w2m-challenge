import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperHeroeFormComponent } from './super-heroe-form.component';

describe('SuperHeroeFormComponent', () => {
  let component: SuperHeroeFormComponent;
  let fixture: ComponentFixture<SuperHeroeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SuperHeroeFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SuperHeroeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
