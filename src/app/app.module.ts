import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { HeaderModule } from './sections/header/header.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { SharedModule } from './sections/shared/shared.module';
import { ListModule } from './sections/list/list.module';
import { HomeModule } from './sections/home/home.module';
import { ConfirmDialogModule } from './sections/confirm-dialog/confirm-dialog.module';
import { SuperHeroeFormModule } from './sections/super-heroe-form/super-heroe-form.module';
import { LoaderComponent } from './sections/loader/loader.component';

registerLocaleData(localeEs);


@NgModule({
  declarations: [
    AppComponent,
    LoaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HeaderModule,
    ListModule,
    HomeModule,
    SharedModule,
    HttpClientModule,
    ConfirmDialogModule,
    SuperHeroeFormModule
  ],
  providers: [
    provideClientHydration(),
    {provide: LOCALE_ID, useValue: 'es'},
    provideAnimationsAsync()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
